function finAlmuerzo(employee){
  asistencias.get(dateTime.fecha+"-"+dni ).then(function(doc) {
    doc.inicio_refrigerio = dateTime.hora;
    doc.couchname = "asistencias";
    doc.from = "app";
    doc.db = "asistencias";
    return asistencias.put( doc );
  }).then(function(response){
      localStorage.setItem("AF:" + dateTime.fecha + ":" + employee.dni, "Fin");
      Swal.fire({
          title: "Retomemos el trabajo",
          text: employee.employee_name,
          type: "success",
          icon: 'success',
          showCancelButton: false,
          showConfirmButton: false,
          timer: 3000
      });
      $("#dni").val("");
  }).catch(function (err)  {
        console.log(err); 
        Swal.fire({
            title: "Error",
            text: "Ha ocurrido un error al grabar sus datos por favor consulte con soporte",
            type: "error",
            icon: 'error',
            showCancelButton: false,
            showConfirmButton: false,
            timer: 3000
        });
    });
}