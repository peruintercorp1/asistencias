var utilidades = {
    
    getDateTime(){

        var d = new Date();
        d = new Date(d.getTime() );
        var date_format_str = d.getFullYear().toString()+"-"+((d.getMonth()+1).toString().length==2?(d.getMonth()+1).toString():"0"+(d.getMonth()+1).toString())+"-"+(d.getDate().toString().length==2?d.getDate().toString():"0"+d.getDate().toString());
        var hour_format_str =(d.getHours().toString().length==2?d.getHours().toString():"0"+d.getHours().toString())+":"+(d.getMinutes().toString().length==2?d.getMinutes().toString():"0"+d.getMinutes().toString())+":"+(d.getSeconds().toString().length==2? d.getSeconds().toString():"0"+d.getSeconds().toString());
        
        return {
            fecha:date_format_str,
            hora:hour_format_str
        }
    },

    checkDni(){
        if(query_doc.startsWith("*"))
        {
            nnn=9;
        }
        if(query_doc.length != nnn)
        {
            Swal.fire({
                title: "DNI incompleto",
                text: "Ingrese un DNI de 8 caracteres.",
                type: "error",
                icon: 'error',
                showCancelButton: false,
                showConfirmButton: false,
                timer: 3000
            });
            return "error";
        } else {
            return query_doc.replace("*","");
        }
    },
    ErrorEmpleadoNoEncontrado(){
        return Swal.fire({
            title: "Empleado no encontrado",
            text: "El DNI del empleado no se ha podido encontrar por favor vuelva a ingresar su DNI.",
            icon: 'error',
            showCancelButton: false,
            showConfirmButton: false,
            timer: 3000,
        });
    }
}


function Numeros(string){

    var out = '';
    var filtro = '1234567890*';
    var nnn=8;
    if(string.startsWith("*"))
    {
        nnn=9;
    }
    for (var i=0; i<nnn; i++)
    if (filtro.indexOf(string.charAt(i)) != -1)
        out += string.charAt(i);
    return out;

}



function startTime(){
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('txt').innerHTML =
    h + ":" + m + ":" + s;
    var t = setTimeout(startTime, 500);
}
function checkTime(i)  {
    if (i < 10) {i = "0" + i};
    return i;
}