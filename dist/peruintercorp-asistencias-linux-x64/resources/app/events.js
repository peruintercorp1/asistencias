function entrada_salida(e){
    
    if(e.keyCode == 13){
        let ES_Storage="Vacio";
        let AF_Storage="Vacio";
        nnn=8;
        
        query_doc = $("#dni").val();
        dateTime = utilidades.getDateTime();
        dni = utilidades.checkDni();
        if( dni == "error" ){
            return;
        }
        var newquery = query_doc.replace("*","");
        console.log(newquery)
        empleado.find({
            selector: {dni: { $eq: newquery }}
        }).then(function (result) {
            if( result.docs.length == 0 ){
                utilidades.ErrorEmpleadoNoEncontrado();
                $("#dni").val("");
                return;
            }else{
                if(result.docs[0].status != "Active"){
                    utilidades.ErrorEmpleadoNoEncontrado();
                    $("#dni").val("");
                    return;
                }
            }
            employee = result.docs[0];
            
            if(localStorage.getItem( "ES:" + dateTime.fecha + ":" + employee.dni ) ){
                ES_Storage = localStorage.getItem( "ES:" + dateTime.fecha + ":" + employee.dni );
            }
            if(localStorage.getItem( "AF:" + dateTime.fecha + ":" + employee.dni ) ){
                AF_Storage = localStorage.getItem( "AF:" + dateTime.fecha + ":" + employee.dni );
            }
            if(nnn == 8){
                if(ES_Storage == "Vacio"){
                    entrada(employee);
                }else{
                    salida(employee);
                }
            }else{
                if(AF_Storage == "Vacio"){
                    initAlmuerzo(employee);
                }else{
                    finAlmuerzo(employee);
                }
            }

        });

  
        

    }
}