function entrada(employee){
    asistencias.put({
        couchname:"asistencias",
        from:"app",
        db:"asistencias",
        _id:dateTime.fecha+"-"+employee.dni,
        dni:""+employee.dni,
        name:"HR-ATT-"+dateTime.fecha+"-"+employee.dni,
        naming_series:"HR-ATT-.YYYY.-.MM.-.DD.-.########",
        employee: employee.name,
        employee_name: employee.employee_name,
        status: "Present",
        docstatus:1,
        attendance_date: dateTime.fecha,
        hora_entrada: dateTime.hora,
        company: employee.company
    }).then(function(response){
        localStorage.setItem("ES:" + dateTime.fecha + ":" + employee.dni, "Entrada");
        Swal.fire({
            title: "Buenos días",
            text: employee.employee_name,
            type: "success",
            icon: 'success',
            showCancelButton: false,
            showConfirmButton: false,
            timer: 3000
        });
        $("#dni").val("");
    }).catch(function (err)  {
        console.log(err); 
        Swal.fire({
            title: "Error",
            text: "Ha ocurrido un error al grabar sus datos por favor consulte con soporte",
            type: "error",
            icon: 'error',
            showCancelButton: false,
            showConfirmButton: false,
            timer: 3000
        });
    });
}
              