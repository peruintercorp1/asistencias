

empleado.replicate
  .from
  (empleadoRemote, {
    live: true,
    retry: true

  }).on('complete', function (info) {
  }).on('change', function (info) {
  }).on('paused', function (info) {
  }).on('active', function (info) {
  }).on('error', function (err) { });


asistencias.sync
  (asistenciasRemote, {
    live: true,
    retry: true

  }).on('complete', function (info) {
  }).on('change', function (info) {
  }).on('paused', function (info) {
  }).on('active', function (info) {
  }).on('error', function (err) {
  });



document.onkeydown = function (e) {
  e = e || window.event;
  showCapsLockMsg(e);
}
document.onload = function (e) {
  showCapsLockMsg(e);
}

function showCapsLockMsg(e) {
  if (e.keyCode == 144) {
    if (e.getModifierState('NumLock') == true) {
      Swal.fire({
        title: "Bloq Num Desactivado",
        text: "Por favor presione la tecla Bloq Num y Revise que el led del teclado numérico se encuentre activo",
        type: "error",
        icon: 'success',
        showCancelButton: false,
        showConfirmButton: false,
        timer: 3000
      });
    }
  } else {
    if (e.getModifierState('NumLock') == false) {
      Swal.fire({
        title: "Bloq Num Desactivado",
        text: "Por favor presione la tecla Bloq Num y Revise que el led del teclado numérico se encuentre activo",
        type: "error",
        icon: 'success',
        showCancelButton: false,
        showConfirmButton: false,
        timer: 3000
      });
    }
  }
}
