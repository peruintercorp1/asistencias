function salida(employee){
  asistencias.get(dateTime.fecha+"-"+dni ).then(function(doc) {
    doc.hora_salida = dateTime.hora;
    doc.couchname = "asistencias";
    doc.from = "app";
    doc.db = "asistencias";
    return asistencias.put( doc );
  }).then(function(response){
    localStorage.setItem("ES:" + dateTime.fecha + ":" + employee.dni, "Salida");
    console.log( dateTime.semana );
    if(dateTime.semana == "Sábado"){
      Swal.fire({
        title: "Que tengas un buen fin de semana",
        text: "Hasta el lunes "+employee.employee_name,
        type: "success",
        icon: 'success',
        showCancelButton: false,
        showConfirmButton: false,
        timer: 3000
      });
    }else{
      Swal.fire({
          title: "Adiós",
          text: employee.employee_name,
          type: "success",
          icon: 'success',
          showCancelButton: false,
          showConfirmButton: false,
          timer: 3000
      });
    }
      
      $("#dni").val("");
  }).catch(function (err) { 
    console.log(err);
    Swal.fire({
      title: "Error",
      text: "Ha ocurrido un error al grabar sus datos por favor consulte con soporte",
      type: "error",
      icon: 'error',
      showCancelButton: false,
      showConfirmButton: false,
      timer: 3000
    });
});

}